import Web3 from 'web3'
import {address, ABI} from './constants/dappContract'

let getContract = new Promise(function (resolve, reject) {
  let web3 = new Web3(window.web3.currentProvider)
  let dappContract = web3.eth.contract(ABI)
  let dappContractInstance = dappContract.at(address)
  console.log(dappContract)
  console.log(dappContractInstance)
  resolve(dappContractInstance)
})

export default getContract
