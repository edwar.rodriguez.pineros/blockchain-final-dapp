import Vue from 'vue'
import Router from 'vue-router'
import BlockchainFinalDapp from '@/components/blockchain-final-dapp'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'blockchain-final-dapp',
      component: BlockchainFinalDapp
    }
  ]
})
