pragma solidity ^0.4.10;

import "./Ownable.sol";
import "./Mortal.sol";

contract Savings is Mortal{
    
 uint minBalance = 0;
 uint minDepositValue = 1;
 
 constructor() payable public {
    }
    
    function deposit() payable public {
        require(msg.value >= minDepositValue);
        require(msg.sender.balance >= msg.value);
    }
    
    function withdraw(uint amount) Owned public {
        require(address(this).balance > amount);
        if(!msg.sender.send(amount)) 
            revert();
    }
    
    function() public { //fallback
        revert();
    }
    
    function checkContractBalance() Owned public view returns(uint) {
        return address(this).balance;
    }
    
    function checkContractBalanceInEther() Owned public view returns (uint) {
        return checkContractBalance() / 1e18;
    }  
    
}