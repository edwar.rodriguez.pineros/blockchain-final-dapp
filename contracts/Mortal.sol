pragma solidity ^0.4.10;

import "./Ownable.sol";

contract Mortal is Ownable {
//Our access modifier is present, only the contract creator can      use this function
 function kill() public Owned { 
    selfdestruct(owner);
 }
 
}